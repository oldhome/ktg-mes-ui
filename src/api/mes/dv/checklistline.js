import request from '@/utils/request'

// 查询点检保养单行列表
export function listCheckline(query) {
  return request({
    url: '/mes/qc/maintenanceline/list',
    method: 'get',
    params: query
  })
}

// 查询点检保养单行详细
export function getCheckline(lineId) {
  return request({
    url: '/mes/qc/maintenanceline/' + lineId,
    method: 'get'
  })
}

// 新增点检保养单行
export function addCheckline(data) {
  return request({
    url: '/mes/qc/maintenanceline',
    method: 'post',
    data: data
  })
}

// 修改点检保养单行
export function updateCheckline(data) {
  return request({
    url: '/mes/qc/maintenanceline',
    method: 'put',
    data: data
  })
}

// 删除点检保养单行
export function delCheckline(lineId) {
  return request({
    url: '/mes/qc/maintenanceline/' + lineId,
    method: 'delete'
  })
}
