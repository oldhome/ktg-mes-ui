import request from '@/utils/request'

// 查询点检保养单列表
export function listCheckList(query) {
  return request({
    url: '/mes/qc/maintenance/list',
    method: 'get',
    params: query
  })
}

// 查询点检保养单详细
export function getCheck(inspectionId) {
  return request({
    url: '/mes/qc/maintenance/' + inspectionId,
    method: 'get'
  })
}

// 新增点检保养单
export function addCheck(data) {
  return request({
    url: '/mes/qc/maintenance',
    method: 'post',
    data: data
  })
}

// 修改点检保养单
export function updateCheck(data) {
  return request({
    url: '/mes/qc/maintenance',
    method: 'put',
    data: data
  })
}

// 完成点检保养单
export function finishCheck(data) {
  return request({
    url: '/mes/qc/maintenance/finish',
    method: 'post',
    data: data
  })
}

// 删除点检保养单
export function delCheck(inspectionIds) {
  return request({
    url: '/mes/qc/maintenance/' + inspectionIds,
    method: 'delete'
  })
}


// 验收 点检保养单
export function checkCheck(id,bool) {
  return request({
    url: '/mes/qc/maintenance/check?inspectionId='+id+'&bool='+bool+'',
    method: 'post',
    // data: data
  })
}
