import request from '@/utils/request'

// 查询流转单列表
export function listTransorder(query) {
  return request({
    url: '/mes/pro/transorder/list',
    method: 'get',
    params: query
  })
}

// 查询流转单详细
export function getTransorder(transOrderId) {
  return request({
    url: '/mes/pro/transorder/' + transOrderId,
    method: 'get'
  })
}

// 新增流转单
export function addTransorder(data) {
  return request({
    url: '/mes/pro/transorder',
    method: 'post',
    data: data
  })
}

// 修改流转单
export function updateTransorder(data) {
  return request({
    url: '/mes/pro/transorder',
    method: 'put',
    data: data
  })
}

// 删除流转单
export function delTransorder(transOrderId) {
  return request({
    url: '/mes/pro/transorder/' + transOrderId,
    method: 'delete'
  })
}
