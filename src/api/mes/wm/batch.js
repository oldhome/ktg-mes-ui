import request from '@/utils/request'

// 查询批次码列表
export function listBatch(query) {
  return request({
    url: '/mes/wm/batch/list',
    method: 'get',
    params: query
  })
}

// 查询批次码详细
export function getBatch(batchId) {
  return request({
    url: '/mes/wm/batch/' + batchId,
    method: 'get'
  })
}

// 新增批次码
export function addBatch(data) {
  return request({
    url: '/mes/wm/batch',
    method: 'post',
    data: data
  })
}

// 修改批次码
export function updateBatch(data) {
  return request({
    url: '/mes/wm/batch',
    method: 'put',
    data: data
  })
}

// 删除批次码
export function delBatch(batchId) {
  return request({
    url: '/mes/wm/batch/' + batchId,
    method: 'delete'
  })
}
