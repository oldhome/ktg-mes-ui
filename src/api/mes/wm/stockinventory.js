import request from '@/utils/request'

// 查询库存盘点列表
export function listStockinventory(query) {
  return request({
    url: '/mes/wm/wmstockpan/list',
    method: 'get',
    params: query
  })
}

// 查询库存盘点详细
export function getStockinventoryDetail(panId) {
  return request({
    url: '/mes/wm/wmstockpan/' + panId,
    method: 'get'
  })
}

// 新增物料入库单
export function addStockinventory(data) {
  return request({
    url: '/mes/wm/wmstockpan',
    method: 'post',
    data: data
  })
}

// 修改物料入库单
export function updateStockinventory(data) {
  return request({
    url: '/mes/wm/wmstockpan',
    method: 'put',
    data: data
  })
}

// 删除物料入库单
export function delStockinventory(StockPanIds) {
    return request({
      url: '/mes/wm/wmstockpan/' + StockPanIds,
      method: 'delete'
    })
  }
  

//执行库存调整 
export function execute(panId) {
  return request({
    url: '/mes/wm/wmstockpan/' + panId,
    method: 'put'
  })
}


