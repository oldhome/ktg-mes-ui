import request from '@/utils/request'

// 查询库存盘点行列表
export function listStockinventoryline(query) {
  return request({
    url: '/mes/wm/stockPanLine/list',
    method: 'get',
    params: query
  })
}
 
// 查询库存盘点列表
export function listStockinventory1(query) {
  return request({
    url: '/mes/wm/wmstockpan/list1',
    method: 'get',
    params: query
  })
}
// // 查询库存盘点行详细
// export function getStockinventoryline(lineId) {
//   return request({
//     url: '/mes/wm/productrecptline/' + lineId,
//     method: 'get'
//   })
// }

// 新增库存盘点行
export function addStockinventoryline(data) {
  return request({
    url: '/mes/wm/stockPanLine',
    method: 'post',
    data: data
  })
}

// 修改库存盘点行
export function updateStockinventoryline(data) {
  return request({
    url: '/mes/wm/stockPanLine',
    method: 'put',
    data: data
  })
}

// 删除库存盘点行
export function delStockinventoryline(Ids) {
  return request({
    url: '/mes/wm/stockPanLine/' + Ids,
    method: 'delete'
  })
}
